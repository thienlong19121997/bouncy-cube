﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public Color[] colorBouncys;

    public GameObject targetBouncy;
    private float positionX;

    public string NameGame;
    public string BundleID;
    public GameObject outLine;

    void Awake(){
		instance = this;
        Application.targetFrameRate = 60;
	}

	void Start(){

	}
		
	public void SpawnObject (int number,float scaleX,bool isPlayer){
		GameObject objectClone = ObjectBouncyController.instance.GetObject ();


        if (targetBouncy == null)
        {
            positionX = 0.0f ;
        }
        else
        {
            positionX = targetBouncy.transform.position.x + (1.9f * targetBouncy.transform.localScale.x/2) + (1.9f * scaleX /2);
        }

        Vector3 temp = objectClone.transform.GetChild(0).gameObject.transform.localScale;
        temp.x = scaleX ;
        objectClone.transform.GetChild(0).gameObject.transform.localScale = temp;
        objectClone.transform.position = new Vector2 (positionX, objectClone.transform.position.y);

		int randomObject = Random.Range (0, objectClone.transform.childCount);

		for (int k = 0; k < objectClone.transform.childCount; k++) {
			if (k == randomObject) {
				objectClone.transform.GetChild (k).gameObject.SetActive (true);
				int valueNumber = number;
				objectClone.transform.GetChild (k).gameObject.GetComponent<Bouncy> ().Init (valueNumber, isPlayer);
            } else {
				objectClone.transform.GetChild (k).gameObject.SetActive (false);
			}
		}
			

		objectClone.SetActive (true);

        targetBouncy = objectClone.transform.GetChild(0).gameObject;

    }


   






}
