﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.NiceVibrations;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using System.Text.RegularExpressions;
public class Player : MonoBehaviour {

    public bool isResetData;

	public static Player instance;
    public Animator changeMapAnim;
    public GameObject MenuInGame;
    public GameObject tutorial;
    public Text coinText;

    public GameObject arrow1, arrow2;

    public int GetCoin()
    {
        return PlayerPrefs.GetInt("coin");
    }

    public void SetCoin(int number)
    {
        int myCoin = GetCoin();
        myCoin += number;
        PlayerPrefs.SetInt("coin", myCoin);
        if(myCoin > 1000000)
        {

            int a = (int)(myCoin / 1000000);
            int b = (int)(myCoin % 1000000);
            string s = b.ToString();
            char[] myChars = s.ToCharArray();
    
            if(myChars.Length == 0)
            {
                coinText.text = a + "." + myChars[0].ToString()  + "m";

            }
            else if (myChars.Length == 1)
            {
                coinText.text = a + "." + myChars[0].ToString() + myChars[1].ToString() + "m";

            }
            else
            {
                coinText.text = a + "." + myChars[0].ToString() + myChars[1].ToString() + myChars[2].ToString() + "m";

            }



        }
        else
        {
            coinText.text = myCoin.ToString();
        }
    }

    [Header("Score")]
    public GameObject positionCoinPlus;
    public Text myScoreTextComplete;
    int currentCoin;
    public GameObject bonusCoinPlusText;
    public GameObject newHighScore;
    public GameObject btnCollect;
    public GameObject completeMenu;
    private int coinBonus;
    public Text coinBonusText;

	public int myNumber;
	public Text myNumberText;

    public int combox;


    private float positionX_player;
    public RectTransform myScoreObject;
    public float myScore;
    public Text textMyScore;

    public RectTransform myBestScoreObject;
    public float myBestScore;
    public Text textMyBestScore;

    bool isTargetScore;
    public float myTargetScore;
    public Text textMyTargetScore;

    public Image barScore;

	[Header("Physic 2D")]
	public Vector2 forceStart;
	public Vector2 forceBouncy;


	[Header("Component")]
	public TrailRenderer myTrail;
	public SpriteRenderer mySpr;
	private Rigidbody2D rig2d;
	private CircleCollider2D cir2d;

    [Header("Info")]
	public float combo;
	public bool isCanTap;
	private bool isBouncy;
	private bool complete;
    public bool isTutorial;
    private int numberTapTutorial;
    public bool isForceStart;
	bool isRun;
	int timesDetectGround;

	private float targetPositionX;
	private const int life =  0;

	[Header("Camera - Air")]
	public float minSize;
	public float maxSize;
	public float minPosY;
	public float maxPosY;

	[Header("Trail")]
	public GameObject[] trails;

    [Header("Map")]
    public GameObject map;
    private int myMap;

    public GameObject[] parentEffects;

    private List<GameObject> maps = new List<GameObject>();
    private List<ElementMap> elementMap = new List<ElementMap>();
    public class ElementMap
    {
        public List<SpriteRenderer> elements = new List<SpriteRenderer>();
    }


	private void Awake(){



        GameAnalytics.Initialize();

        if (isResetData)
        {
            PlayerPrefs.DeleteAll();
        }
        instance = this;
		MMVibrationManager.iOSInitializeHaptics ();

        if(PlayerPrefs.GetInt("firstplay") == 0)
        {
            PlayerPrefs.SetInt("firstplay",1);
            PlayerPrefs.SetFloat("mytargetscore", 300);
            PlayerPrefs.SetInt("levelstartforce", 1);
            PlayerPrefs.SetInt("levelbouncy", 1);
            myBestScoreObject.gameObject.SetActive(false);
        }
        else
        {
            myBestScoreObject.gameObject.SetActive(true);
        }



    

        if (PlayerPrefs.GetInt("isTutorial") == 0)
        {
            isTutorial = true;

        }

    

    }

	private void Start () {
		rig2d = GetComponent<Rigidbody2D> ();
		cir2d = GetComponent<CircleCollider2D> ();

        for (int i = 0; i < map.transform.childCount; i++)
        {
            maps.Add(map.transform.GetChild(i).gameObject);
            elementMap.Add(new ElementMap());
             for(int k = 0; k < maps[i].gameObject.transform.childCount - 1; k++)
            {
                for(int j = 0; j < maps[i].transform.GetChild(k).transform.childCount; j++)
                {
                    elementMap[i].elements.Add(maps[i].transform.GetChild(k).transform.GetChild(j).gameObject.GetComponent<SpriteRenderer>());
                }

            }
        }

  

        InitGame();
    }

    private void InitGame()
    {
        SetCoin(0);

        myMap = -1;
        SetMap(Random.Range(0, maps.Count));

        for (int i = 0; i < 25; i++)
        {
            SpawnObjectBouncy();
        }
        targetLine = Random.Range(4,7);
        combox = 0;
        combo = 0;
        rig2d.gravityScale = 0;
        isBouncy = false;
        complete = false;
        isRun = false;

        // set value score ui

        positionX_player = transform.position.x;

        barScore.fillAmount = 0;

        myScore = 0;
        textMyScore.text = "" + (int)myScore;

        myBestScore = PlayerPrefs.GetFloat("mybestscore");
        textMyBestScore.text = "" + (int)myBestScore;


        if (myBestScore > PlayerPrefs.GetFloat("mytargetscore"))
        {
            myTargetScore = myBestScore + myBestScore * .3f;
            PlayerPrefs.SetFloat("mytargetscore",myTargetScore);
            textMyTargetScore.text = "" + (int)myTargetScore;
        }
        else
        {
            myTargetScore = PlayerPrefs.GetFloat("mytargetscore");
            textMyTargetScore.text = "" + (int)myTargetScore;
        }

        myBestScoreObject.anchoredPosition = new Vector2((myBestScore / myTargetScore * 275 * 2) - 275, myBestScoreObject.anchoredPosition.y);


    }

    public void PlayGame(){
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game");
        FacebookAnalytics.Instance.LogGame_startEvent(1);
        MenuInGame.SetActive(true);

        StartCoroutine (Force_Start ());
	}

	private void Update(){

        if (Input.GetMouseButtonDown(1))
        {
            //PlayerPrefs.DeleteAll();

        }
		if (complete)
			return;
		
		GravityController ();
		CameraControl ();
        ScoreController();
        RotateDirection ();

        //if (isRun)
        //{
        //    float speedRotate;
        //    speedRotate = 500;
        //    transform.Rotate(0, 0, speedRotate * Time.deltaTime);
        //}



	}

    private void ScoreController()
    {
        myScore = transform.position.x - positionX_player;
        textMyScore.text = "" + (int)myScore;

        if(myScore > PlayerPrefs.GetFloat("mybestscore"))
        {
            PlayerPrefs.SetFloat("mybestscore", myScore);
            textMyBestScore.text = "" + (int)myScore;
            myBestScoreObject.gameObject.SetActive(false);
        }

        if(myScore > PlayerPrefs.GetFloat("mytargetscore"))
        {
            if (isTargetScore == false)
            {
                
                isTargetScore = true;
                newHighScore.SetActive(true);
               // SoundManager.instance.PlayAudioClip_Win();
                SetMap(RandomMap());
                Debug.Log(" vuot target score");
            }
        }
        

        barScore.fillAmount = myScore / PlayerPrefs.GetFloat("mytargetscore");

        myScoreObject.anchoredPosition = new Vector2(Mathf.Lerp(-275,275,barScore.fillAmount), myScoreObject.anchoredPosition.y);

    }

    private void GravityController(){
		if (rig2d.gravityScale == 0 )
			return;

		if (rig2d.velocity.y > 0) {
			if (rig2d.gravityScale != 1) {
				rig2d.gravityScale = 1;
			}
		} else {

            if (isForceStart)
            {
                if (rig2d.gravityScale <1)
                {
                    rig2d.gravityScale += Time.deltaTime/10;
                }
                return;
            }

			if (rig2d.gravityScale != 1.5f) {
				rig2d.gravityScale = 1.5f + Mathf.Lerp(0,.5f, combox/15)	;
			}
		}
	}

	private void LateUpdate(){


		Vector3 cameraPositon = Camera.main.transform.position;
		cameraPositon.x = transform.position.x + 5 + (Camera.main.orthographicSize - minSize)/2;

		cameraPositon.y = 1 + (Camera.main.orthographicSize - minSize);

		Camera.main.transform.position = cameraPositon;
	}

	IEnumerator Force_Start(){
        isForceStart = true;

        ChangeTrail (0);
		yield return new WaitForSeconds (0.0f);
		rig2d.gravityScale = 1;
		isRun = true;



        if (isTutorial)
        {
 
            Vector2 forceFixed = new Vector2(forceStart.x, forceStart.y);
            ForcePlayer(forceFixed);
            yield return new WaitForSeconds(.9f);
            Time.timeScale = 0;
            isCanTap = true;
            tutorial.SetActive(true);
            arrow1.SetActive(true);
            arrow2.SetActive(false);
            yield return new WaitForSeconds(1.35f);
            tutorial.SetActive(true);
            arrow1.SetActive(false);
            arrow2.SetActive(true);
            Time.timeScale = 0;
            isCanTap = true;

        }
        else
        {
            float levelForceStart = Mathf.Lerp(0, 1.5f, (float)UIManager.instance.level_startforce / 30.0f);
            Vector2 forceFixed = new Vector2(forceStart.x * (1 + levelForceStart), forceStart.y * (1 + levelForceStart));
            ForcePlayer(forceFixed);
            yield return new WaitForSeconds(.2f);
            isCanTap = true;
        }



	}

	public void Force_Tap(){



		if (isCanTap == false || complete)
			return;


        Debug.Log(" TAP  TAP ");

        Debug.Log(rig2d.velocity.y);

        if (isForceStart)
        {
            float levelForceStart = Mathf.Lerp(0, 30, (float)UIManager.instance.level_startforce / 30.0f);
            Debug.Log(levelForceStart + "AS");
            Vector2 forceTapFixed = new Vector2(rig2d.velocity.x * 1f, -25 - (levelForceStart / 2 * 2.5f));
            ForcePlayer(forceTapFixed);
        }
        else
        {
            
            Vector2 forceTapFixed = new Vector2(rig2d.velocity.x * 1.2f, -25 - (Mathf.Lerp(0,15, combo / 20) * 2.5f));
            ForcePlayer(forceTapFixed);

        }

        Debug.Log(rig2d.velocity.y);

        //Debug.Break();

        isCanTap = false;

        if (isTutorial)
        {
            numberTapTutorial++;
            tutorial.SetActive(false);
            Time.timeScale = 1;
            if(numberTapTutorial == 2)
            {
                isTutorial = false;
                PlayerPrefs.SetInt("isTutorial", 2);
            }
      
        }
	}


	void ForcePlayer(Vector2 velocity){
		rig2d.velocity = velocity;
	}
		

	void RotateDirection(){
		Vector2 dir = rig2d.velocity;
		float an = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
		var newRotation = Quaternion.Euler (0, 0, an);
		transform.rotation = Quaternion.Lerp(transform.rotation, newRotation , 10*Time.deltaTime);
	}

	void CameraControl(){
		float t = (transform.position.y - minPosY) / (maxPosY - minPosY);
		float orthoSize = Mathf.Lerp (minSize, maxSize, t);
		Camera.main.orthographicSize = orthoSize;
	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Line")
        {
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
          //  SoundManager.instance.PlayAudioClip_EnterLine();

            Debug.Log(" hit line => change color");
            SetMyNumber(collision.gameObject.GetComponent<Line>().number);
            UpdateColorOfBouncy();
        }
    }


    void OnCollisionEnter2D(Collision2D col){
		if (complete)
			return;

        Debug.Log("col");
        if(col.gameObject.tag == "Ground")
        {
            CompleteGame();
            return;
        }

		if (col.gameObject.tag == "Bouncy" && isBouncy == false) {
          //  StartCoroutine(CoinEffect());
            MMVibrationManager.Haptic(HapticTypes.LightImpact);

            if (isForceStart)
            {
                isForceStart = false;
            }
			EffectSurface ();
			EffectRipple (col.gameObject.transform.position,col.gameObject.GetComponent<SpriteRenderer>().color);
			Debug.Log ("BOUNCY");
            isBouncy = true;

            int colNumber = col.gameObject.GetComponent<Bouncy>().number;

            if (myNumber == colNumber) {
                col.gameObject.SetActive(false);
                SpawnLine(transform.position.x + Mathf.Lerp(30,100,combo/15));

                SetCombox(true);
                if (combo < 14)
				combo++;
            }
            else
            {
                CompleteGame();
                return;
            }
            float cfix = Mathf.Lerp(0, 15, (float)combo / (20.0f - Mathf.Clamp(0,5,(float)UIManager.instance.level_bouncy/20.0f)));
			Vector2 forceBouncyFixed = forceBouncy + new Vector2 ((1 + cfix * 1.5f), (1 + cfix * 1.1f));
			ForcePlayer (forceBouncyFixed);
			StartCoroutine (SetBouncy ());

			timesDetectGround = 0;

            //int coin = 2 * (int)combo + 1;
            int coin = (int)(Mathf.Pow(2, (combo)));
            currentCoin += coin;
            SetCoin(coin);
            GameObject scorePlus = ScorePlusPooler.instance.GetObject();
            scorePlus.transform.position = col.GetContact(0).point;
           
            scorePlus.transform.GetChild(0).gameObject.GetComponent<Text>().text = "+" + coin;
            scorePlus.SetActive(true);

        } 
	}
		
    IEnumerator CoinEffect()
    {
        for (int i = 0; i < 3; i++)
        {
            SoundManager.instance.PlayAudioClip_Upgrade();
            yield return new WaitForSeconds(.2f);
        }
    }

	IEnumerator SetBouncy(){
        if (isTutorial)
        {
            yield return new WaitForSeconds(.2f);
            isBouncy = false;
            yield break;
                
        }

        isBouncy = true;
        isCanTap = false;
		yield return new WaitForSeconds (.2f);
		isBouncy = false;
		isCanTap = true;

	}

	void CompleteGame(){
        StartCoroutine(StepComplete());
	}

    IEnumerator StepComplete()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "game", (int)myScore);
        FacebookAnalytics.Instance.LogGame_endEvent(1);
        FacebookAnalytics.Instance.LogGame_end_endlessEvent(1);

        MMVibrationManager.Haptic(HapticTypes.Success);
        Debug.Log(" complete ");
        complete = true;


        Vector2 forceBouncyFixed = new Vector2(7 + Mathf.Lerp(1, 6, combo / 15), 5 + Mathf.Lerp(1, 4, combo / 15));
        ForcePlayer(forceBouncyFixed);
        rig2d.gravityScale = 1;


        yield return new WaitForSeconds(1.5f);
        // ShakeCamera();
        myScoreTextComplete.text = ((int)myScore).ToString();
        coinBonus = 1+ (int)(currentCoin/3);
        coinBonusText.text = coinBonus.ToString();
        completeMenu.SetActive(true);

        yield return new WaitForSeconds(1.5f);



        if (coinBonus < 10)
        {
            SetCoin(coinBonus);
            GameObject scorePlus = ScorePlusPooler.instance.GetObject();
            scorePlus.transform.position = positionCoinPlus.transform.position;
            scorePlus.transform.localScale = new Vector3(.7f, .7f, .7f);
            scorePlus.transform.GetChild(0).gameObject.GetComponent<Text>().text = "+" + (int)coinBonus;
            scorePlus.SetActive(true);
          //  SoundManager.instance.PlayAudioClip_CoinPop();

        }
        else
        {
            float c = (float)coinBonus / 5.0f;
            float d = (float)coinBonus % 5.0f;
            Debug.Log(" c : " + c + " __ d : " + d);
            for (int i = 0; i < 6; i++)
            {
                if(i < 5)
                {
                    
                    SetCoin((int)c);
                    SetCoin((int)d);
                    GameObject scorePlus = ScorePlusPooler.instance.GetObject();
                    scorePlus.transform.position = positionCoinPlus.transform.position;
                    scorePlus.transform.localScale = new Vector3(.7f, .7f, .7f);
                    scorePlus.transform.GetChild(0).gameObject.GetComponent<Text>().text = "+" + (int)c;
                    scorePlus.SetActive(true);

                }
                else
                {
                    SetCoin((int)d);
                    GameObject scorePlus = ScorePlusPooler.instance.GetObject();
                    scorePlus.transform.position = positionCoinPlus.transform.position;
                    scorePlus.transform.localScale = new Vector3(.7f, .7f, .7f);

                    scorePlus.transform.GetChild(0).gameObject.GetComponent<Text>().text = "+" + (int)d;
                    scorePlus.SetActive(true);

                }
                Debug.Log("++ coin");
               // SoundManager.instance.PlayAudioClip_CoinPop();

                yield return new WaitForSeconds(.2f);
            }
        }

        Invoke("LoadScene", 1.5f);


    }

    //public void OnClickCollectCoinBonus()
    //{
    //    SoundManager.instance.PlayAudioClip_Upgrade();
    //    SetCoin(coinBonus);

    //    bonusCoinPlusText.GetComponent<Text>().text = "+" + coinBonus;
    //    bonusCoinPlusText.SetActive(true);


    //    coinBonus = 0;
    //    coinBonusText.text = coinBonus.ToString();


    //    Invoke("LoadScene", 2.0f);
    //}

    void ShakeCamera()
    {
        iTween.ShakeRotation(Camera.main.gameObject, new Vector3(4, 4, 4), .2f);

    }

    void LoadScene(){
		SceneManager.LoadScene (0);
	}

	void EffectSurface(){
		GameObject objectClone = EffectSurfacePooler.instance.GetObject ();
		objectClone.SetActive (true);
		objectClone.transform.position = transform.position + new Vector3 (0, 0 ,0);
	}

	void EffectRipple(Vector3 position,Color color){
		GameObject objectClone = EffectSurfacePooler.instance.GetObject_Ripple ();
		objectClone.transform.position =  position;
		ParticleSystem.MainModule settings = objectClone.GetComponent<ParticleSystem> ().main;
		settings.startColor = new ParticleSystem.MinMaxGradient( color );
		objectClone.SetActive (true);
	}

	void ChangeTrail(int number){
		for (int i = 0; i < trails.Length; i++) {
			if (i == number) {
				trails [i].SetActive (true);
			} else {
				trails [i].SetActive (false);
			}
		}	
	}

	void SetMyNumber(int num){

		myNumber = num;
		
		myNumberText.text = myNumber.ToString();
        mySpr.color = GameManager.instance.colorBouncys[myNumber];
		myTrail.colorGradient.colorKeys [0].color = GameManager.instance.colorBouncys[myNumber];
        myTrail.colorGradient.colorKeys [1].color = GameManager.instance.colorBouncys[myNumber];

        Debug.Log (myTrail.colorGradient.colorKeys.Length);

		Gradient gradient = new Gradient();
		gradient.SetKeys(
			new GradientColorKey[] { new GradientColorKey(GameManager.instance.colorBouncys[myNumber], 0.0f), new GradientColorKey(Color.white, 1.0f) },
			new GradientAlphaKey[] { new GradientAlphaKey(1, 0.0f), new GradientAlphaKey(0, 1.0f) }
		);
		myTrail.colorGradient = gradient;
	}


    int spawn;
    int targetSpawn;
    int randomSpawn;

    bool isRandomTutorial;

    public void SpawnObjectBouncy(){
        int maxLength = GameManager.instance.colorBouncys.Length;

        if (isTutorial)
        {
            isRandomTutorial = !isRandomTutorial;
            if (isRandomTutorial)
            {
                GameManager.instance.SpawnObject(myNumber, 4, isRandomTutorial);

            }
            else
            {
                for (int i = 0; i < 1; i++)
                {
                    randomSpawn = Random.Range(0, maxLength);
                    if (randomSpawn == myNumber)
                    {
                        i = -1;
                    }

                }
                GameManager.instance.SpawnObject(randomSpawn, 4, isRandomTutorial);

            }

            return;
        }

        if(spawn >= targetSpawn)
        {
            spawn = 0;
            if (combox < 3)
            {
                targetSpawn = Random.Range(1, 4);
            }
            else if (combox < 6)
            {
                targetSpawn = Random.Range(2, 5);
            }
            else if (combox < 10)
            {
                targetSpawn = Random.Range(2, 6);
            }
            else if (combox < 13)
            {
                targetSpawn = Random.Range(3, 6);
            }
            else
            {
                targetSpawn = Random.Range(3, 7);

            }

            float scalex = 3 - Mathf.Lerp(0, .5f, (float)combox / 15.0f);
            GameManager.instance.SpawnObject(myNumber, scalex, true);
        }
        else
        {
            spawn++;
            for(int i = 0; i < 1; i++)
            {
                randomSpawn = Random.Range(0, maxLength);
                if(randomSpawn == myNumber)
                {
                    i = -1;
                }
             
            }
            float scalex = 3 - Mathf.Lerp(0, .5f, (float)combox / 15.0f);
            GameManager.instance.SpawnObject(randomSpawn, scalex, false);
        }

	}


    public int numLine;
    public int targetLine;
    List<int> listLine = new List<int>();
    public void SpawnLine(float posX)
    {
        numLine++;

        if (numLine < targetLine)
            return;

 
        Debug.Log("spawn line");
        GameObject lineClone = LineController.instance.GetObject() as GameObject;
        Vector3 temp = lineClone.transform.localPosition;
        temp.x = posX;
        lineClone.transform.localPosition = temp;

        for (int i = 0; i < 1; i++)
        {
            numLine = Random.Range(0, GameManager.instance.colorBouncys.Length);
           for(int k = 0; k < listLine.Count; k++)
            {
                if(numLine == listLine[k])
                {
                    i = -1;
                    k = listLine.Count;
                }
            }

           if(listLine.Count == 0)
            {
                if(numLine == 0)
                {
                    i = -1;
                }
            }
        }
        listLine.Add(numLine);
        lineClone.GetComponent<Line>().OnAwakeLine(numLine);
        lineClone.SetActive(true);

        if (listLine.Count == GameManager.instance.colorBouncys.Length)
        {
            listLine.Clear();
        }

        numLine = 0;
        targetLine = Random.Range(4, 7);
    } 

    void SetCombox(bool isMiss)
    {
        if (complete)
            return;

        GameObject comboXObject = ComboXPooler.instance.GetObject();
        Text comboxText = comboXObject.GetComponent<Text>();
        

        if (isMiss == false)
        {
            combox = 0;
            comboxText.text = "";
            return;
        }

        combox++;
     

        comboxText.text = "COMBO!\n  " + "x" +combox;

    

        //for(int i = 3; i < 300; i+= 3)
        //{
        //    if(combox == i)
        //    {
        //        ChangeTrail(Random.Range(1,trails.Length));
        //    }
        //}

        if(combo == 5)
        {
            ChangeTrail(1);
        }
        else if (combo == 10)
        {
            ChangeTrail(2);
        }

        if (combox == 1)
        {
            comboxText.text = "AMAZING";

        }
        comboXObject.SetActive(true);
    }

    void UpdateColorOfBouncy()
    {
        ObjectBouncyController.instance.UpdateColorBouncy(myNumber, true);
    }

    void SetMap(int number)
    {
       // MusicManager.instance.ChangeMusic(number);

        for (int i = 0; i < maps.Count; i++)
        {
            if(i == number)
            {
                for(int j = 0; j < parentEffects[i].transform.childCount; j++)
                {
                    parentEffects[i].transform.GetChild(j).gameObject.SetActive(true);
                }
  
                for (int k = 0; k < elementMap[i].elements.Count; k++)
                {
                    elementMap[i].elements[k].enabled = true;
                }
            }
            else
            {
                for (int j = 0; j < parentEffects[i].transform.childCount; j++)
                {
                    parentEffects[i].transform.GetChild(j).gameObject.SetActive(false);
                }

                for (int k = 0; k < elementMap[i].elements.Count; k++)
                {
                    elementMap[i].elements[k].enabled = false;
                }
            }
        }

        myMap = number;
    }

    int randomMap;
    int RandomMap()
    {
        changeMapAnim.SetTrigger("changemap");

        for (int i = 0; i < 1; i++)
        {
            randomMap = Random.Range(0, maps.Count);
            if (randomMap == myMap)
            {
                i = -1;
            }
        }

        return randomMap;
    }
}
