﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {
	public float x,y,z;
	// Update is called once per frame
	void LateUpdate () {
		transform.position = Player.instance.transform.position + new Vector3(x,y,z);
	}
}
