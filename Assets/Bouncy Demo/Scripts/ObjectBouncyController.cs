﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBouncyController : MonoBehaviour
{

    public static ObjectBouncyController instance;

    public List<GameObject> objectList = new List<GameObject>();
    public GameObject objectPrefab;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        for (int i = 0; i < 40; i++)
        {
            GererateObject();
        }
    }

    void GererateObject()
    {
        GameObject objectClone = Instantiate(objectPrefab, transform) as GameObject;
        objectClone.gameObject.SetActive(false);
        objectClone.gameObject.transform.localPosition = Vector2.zero;
        objectList.Add(objectClone);
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            if (objectList[i].activeInHierarchy == false)
            {
                return objectList[i];
            }
        }

        GameObject objectClone = Instantiate(objectPrefab, transform) as GameObject;
        objectClone.gameObject.transform.localPosition = Vector2.zero;
        objectList.Add(objectClone);
        return objectClone;
    }

    void Update()
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            if (objectList[i].activeInHierarchy)
            {
                if (Player.instance.transform.position.x - objectList[i].transform.position.x > 35)
                {
                    objectList[i].SetActive(false);
                    Player.instance.SpawnObjectBouncy();
                }
            }
        }
    }

    int updateColorNotPlayer;
    public void UpdateColorBouncy   (int number,bool isplayer)
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            if (objectList[i].activeInHierarchy)
            {
                if (objectList[i].transform.GetChild(0).gameObject.GetComponent<Bouncy>().isPlayer)
                {
                    objectList[i].transform.GetChild(0).gameObject.GetComponent<Bouncy>().Init(number, isplayer);
                }
                else
                {
                    for(int j = 0; j < 1; j++)
                    {
                        updateColorNotPlayer = Random.Range(0, GameManager.instance.colorBouncys.Length);
                        if(updateColorNotPlayer == number)
                        {
                            j= -1;
                        }
                    }
                    objectList[i].transform.GetChild(0).gameObject.GetComponent<Bouncy>().Init(updateColorNotPlayer, false);
                }



            }
        }
    }

}
