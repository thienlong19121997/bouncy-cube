﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineController : MonoBehaviour {
    public static LineController instance;

    public List<GameObject> objectList = new List<GameObject>();
    public List<Line> scriptsLine = new List<Line>();
    public GameObject objectPrefab;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            GererateObject();
        }
    }

    void GererateObject()
    {
        GameObject objectClone = Instantiate(objectPrefab, transform) as GameObject;
        objectClone.gameObject.SetActive(false);
        objectList.Add(objectClone);
        scriptsLine.Add(objectClone.GetComponent<Line>());
    }

    public GameObject GetObject()
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            if (objectList[i].activeInHierarchy == false)
            {
                return objectList[i];
            }
        }

        GameObject objectClone = Instantiate(objectPrefab, transform) as GameObject;
        objectList.Add(objectClone);
        return objectClone;
    }
}
