﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour {

    public static UIManager instance;

    public int price_forceStart = 5000;
    public int price_bounciness = 5000;
    public int level_bouncy;
    public int level_startforce;

    public Text forceStartText;
    public Text bouncyText;

    public GameObject lockBoost_ForceStart;
    public GameObject lockBoost_bounciness;

    private void Awake()
    {
        instance = this;
        level_bouncy = PlayerPrefs.GetInt("levelbouncy");
        level_startforce = PlayerPrefs.GetInt("levelstartforce");

        forceStartText.text = "LEVEL " + level_startforce;
        bouncyText.text = "LEVEL " + level_bouncy;
        CheckActiveBoost();
    }

    public GameObject mainMenu;


    public void OnClickPlayGame()
    {
        mainMenu.SetActive(false);
        Player.instance.PlayGame();
    }

    public void OnClick_UpgradeBoost_StartForce()
    {
        int coin = PlayerPrefs.GetInt("coin");

   

        if(coin >= price_forceStart)
        {
            Player.instance.SetCoin(-price_forceStart);

            int levelstartforce = PlayerPrefs.GetInt("levelstartforce");
            levelstartforce++;
            PlayerPrefs.SetInt("levelstartforce", levelstartforce);

            level_startforce = levelstartforce;
            forceStartText.text = "LEVEL " + level_startforce;

            SoundManager.instance.PlayAudioClip_Upgrade();
        }

        CheckActiveBoost();
    }

    public void OnClick_UpgradeBoost_Bouncy()
    {
        int coin = PlayerPrefs.GetInt("coin");


        if (coin >= price_bounciness)
        {
            Player.instance.SetCoin(-price_bounciness);

            int levelbouncy = PlayerPrefs.GetInt("levelbouncy");
            levelbouncy++;
            PlayerPrefs.SetInt("levelbouncy", levelbouncy);

            level_bouncy = levelbouncy;
            bouncyText.text = "LEVEL " + level_bouncy;

            SoundManager.instance.PlayAudioClip_Upgrade();


        }
        CheckActiveBoost();
    }

    void CheckActiveBoost()
    {
        int myCoin = PlayerPrefs.GetInt("coin");
        if(myCoin >= price_forceStart)
        {

            lockBoost_ForceStart.SetActive(false);
        }
        else
        {
            lockBoost_ForceStart.SetActive(true);
        }

        if(myCoin >= price_bounciness)
        {
            lockBoost_bounciness.SetActive(false);

        }
        else
        {
            lockBoost_bounciness.SetActive(true);

        }

    }







}
