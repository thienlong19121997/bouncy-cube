﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance;


    public AudioClip upgrade, hit, tap, enterline,win,coinPop;
    private AudioSource audioSource;

    private void Awake()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }
  

    public void PlayAudioClip_Upgrade()
    {
        audioSource.PlayOneShot(upgrade);
    }

    public void PlayAudioClip_Hit()
    {
        audioSource.PlayOneShot(hit);

    }

    public void PlayAudioClip_Tap()
    {
        audioSource.PlayOneShot(tap);

    }


    public void PlayAudioClip_EnterLine()
    {
        audioSource.PlayOneShot(enterline);

    }

    public void PlayAudioClip_Win()
    {
        audioSource.PlayOneShot(win);

    }

    public void PlayAudioClip_CoinPop()
    {
        audioSource.PlayOneShot(coinPop);

    }
}
