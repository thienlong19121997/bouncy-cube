﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectSurfacePooler : MonoBehaviour {
	public static EffectSurfacePooler instance;

	public List<GameObject> objectList = new List<GameObject>();
	public GameObject objectPrefab;

	public List<GameObject> objectList_1 = new List<GameObject>();
	public GameObject objectPrefab_1;

	void Awake(){
		instance = this;
	}

	void Start () {
		for (int i = 0; i < 10; i++) {
			GererateObject ();
		}
	}

	void GererateObject(){
		GameObject objectClone = Instantiate (objectPrefab, transform) as GameObject;
		objectClone.gameObject.SetActive (false);
		objectClone.gameObject.transform.localPosition = Vector2.zero;
		objectList.Add (objectClone);

		GameObject objectClone_1 = Instantiate (objectPrefab_1, transform) as GameObject;
		objectClone_1.gameObject.SetActive (false);
		objectClone_1.gameObject.transform.localPosition = Vector2.zero;
		objectList_1.Add (objectClone_1);
	}

	public GameObject GetObject(){
		for (int i = 0; i < objectList.Count; i++) {
			if (objectList [i].activeInHierarchy == false) {
				return objectList [i];
			}
		}

		GameObject objectClone = Instantiate (objectPrefab, transform) as GameObject;
		objectClone.gameObject.transform.localPosition = Vector2.zero;
		objectList.Add (objectClone);
		return objectClone;
	}

	public GameObject GetObject_Ripple(){
		for (int i = 0; i < objectList_1.Count; i++) {
			if (objectList_1 [i].activeInHierarchy == false) {
				return objectList_1 [i];
			}
		}

		GameObject objectClone = Instantiate (objectPrefab_1, transform) as GameObject;
		objectClone.gameObject.transform.localPosition = Vector2.zero;
		objectList_1.Add (objectClone);
		return objectClone;
	}
}
