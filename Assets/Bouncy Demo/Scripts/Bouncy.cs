﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bouncy : MonoBehaviour {

    public bool isPlayer;
	public int number;
	public Text numberText;

    Vector3 _newPosition;
    public float distance = 1f;
    public float speed = 3;

    public SpriteRenderer spr;
    float centerPosy = -6.375f;
    int direction = 1;
    int isMove;

    private void Awake()
    {


    }
    public void Init(int num,bool isplayer){
        isMove = Random.Range(0, 100);
        isPlayer = isplayer;
		number = num;
		numberText.text = number.ToString ();
        spr.color = GameManager.instance.colorBouncys[number];
        transform.position = new Vector3(transform.position.x, centerPosy, transform.position.z);



        if(isplayer == false)
        {
            if (Player.instance.isTutorial)
            {

                return;
            }

            int r = Random.Range(0, 10);
            if(r < 4)
            {
                gameObject.SetActive(false);
            }
                    
        }
    }

    private void Update()
    {

        if(isMove < 70 || isPlayer == false || Player.instance.isTutorial)
        {
            return;
        }

        if(transform.position.y > centerPosy+ 1)
        {
            direction = -1;
        }
        else if (transform.position.y <= centerPosy)
        {
            direction = 1;

        }
        transform.position += new Vector3(0, direction * Time.deltaTime,0);
    }


}
