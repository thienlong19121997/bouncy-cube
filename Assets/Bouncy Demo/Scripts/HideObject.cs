﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObject : MonoBehaviour {
	public float time;

	void OnEnable(){
		Invoke ("Hide", time);
	}

	void Hide(){
		gameObject.SetActive (false);
	}
}
