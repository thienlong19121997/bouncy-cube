﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public static MusicManager instance;


    public AudioClip[] musics;
    private AudioSource audioSource;

    private void Awake()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }


    public void ChangeMusic(int number)
    {
        audioSource.Stop();
        audioSource.clip = musics[number];
        audioSource.Play();
    }

}
