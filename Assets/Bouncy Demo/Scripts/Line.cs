﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour {

    public int number;

    public SpriteRenderer spr;


    public void OnAwakeLine(int num)
    {
        number = num;
        spr.color = GameManager.instance.colorBouncys[number];
    }
    
}
