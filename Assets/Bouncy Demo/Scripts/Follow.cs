﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {

	public float percent = 1;
	Vector3 offset;
	void Start(){
		offset = transform.position  - Player.instance.transform.position;
	}

	void Update () {
		Vector3 targetPos = transform.position;
		targetPos.x = Player.instance.transform.position.x * percent + offset.x;
		transform.position = targetPos ;
	}
}
