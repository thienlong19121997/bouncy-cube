﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopElement : MonoBehaviour {


	private List<GameObject> listObject = new List<GameObject>();
	Queue queObject = new Queue();
	private float startPositionX;
	public float weight;
	public float targetPositionPlayer;

	void Start () {
		for (int i = 0; i < transform.childCount; i++) {
			listObject.Add (transform.GetChild (i).gameObject);
		}

		startPositionX  = listObject [listObject.Count - 1].transform.localPosition.x;
		weight = listObject [1].transform.localPosition.x - listObject [0].transform.localPosition.x;

		for (int i = 0; i < listObject.Count; i++) {
			queObject.Enqueue (listObject [i]);
		}
		targetPositionPlayer = Player.instance.transform.position.x + (weight);
	}
	

	void Update () {
		if (Player.instance.transform.position.x - transform.position.x >= (targetPositionPlayer)) {
			targetPositionPlayer += (weight);
			startPositionX += weight;
			GameObject target = queObject.Dequeue () as GameObject;
			Vector3 temp = target.transform.localPosition;
			temp.x = startPositionX;
			target.transform.localPosition = temp;
			queObject.Enqueue (target);
		}
	}



}
